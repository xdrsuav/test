document.write('<script type="text/javascript" src="roslib/roslib.min.js"></script>');

function rosInit() {
    this.ros = new ROSLIB.Ros({
        url: 'ws://localhost:9090'
    });

    ros.on('connection', function () {
        console.log('Connected to websocket server.');
    });

    ros.on('error', function (error) {
        console.log('Error connecting to websocket server: ',
            error);
    });

    ros.on('close', function () {
        console.log('Connection to websocket server closed.');
    });

    //订阅消息
    this.gps = new ROSLIB.Topic({
        ros: ros,
        name: '/gps',
        messageType: 'rosmsg/gps'
    });

    gps.subscribe(function (msg) {
        console.log('received msg: ' + msg.lon + ' ' + msg.lat);
        start_xy.lng = msg.lon - LON_OFFSET;
        start_xy.lat = msg.lat - LAT_OFFSET;
        addMarkerStart(msg.lon, msg.lat, "当前位置");
        map.setCenter([msg.lon, msg.lat]);
    });

    this.gps = new ROSLIB.Topic({
        ros: ros,
        name: '/route',
        messageType: 'rosmsg/route'
    });

    gps.subscribe(function (msg) {
        console.log('received msg: ' + msg.lon + ' ' + msg.lat);
        addMarkerStart(msg.lon[0], msg.lat[0], "起点");
        addMarkerStart(msg.lon[msg.lon.length - 1], msg.lat[msg.lat.length - 1], "终点");

        lineArr = new Array();
        for (var i = 0; i < msg.lon.length; i++) {
            lineArr.push(new AMap.LngLat(msg.lon[i] - LON_OFFSET, msg.lat[i] - LAT_OFFSET));
        }

        var polyline = new AMap.Polyline({
            map: map,
            path: lineArr,
            strokeColor: "#0000FF", //线颜色
            strokeOpacity: 1, //线透明度
            strokeWeight: 3, //线宽
            strokeStyle: "solid", //线样式
        });
    });


    //发布消息
    // this.route = new ROSLIB.Topic({
    // 	ros: ros,
    // 	name: '/route',
    // 	messageType: 'rosmsg/route'
    // });
}


function mapInit() {
    this.map = new AMap.Map('mapContainer', {
        resizeEnable: true,
        zoom: 16, //地图显示的缩放级别
        center: [lon, lat] //地图中心点
    });


    AMap.event.addDomListener(document.getElementById('navigate'), 'click',
        function () {
            navigation([start_xy.lng, start_xy.lat], [end_xy.lng, end_xy.lat]);
        });

    this.map.on('click', function (e) {
        end_xy = e.lnglat;
        addMarkerEnd(end_xy.lng, end_xy.lat, "终点");
        document.getElementById("tip").innerHTML = [end_xy.lng, end_xy.lat];
    });
}


function addMarkerStart(lon, lat, str) {
    if (markerStart != null) {
        markerStart.setMap(null);
    }

    markerStart = new AMap.Marker({
        position: [lon, lat],
        icon: "https://webapi.amap.com/theme/v1.3/markers/n/mark_b.png",
    });

    markerStart.setLabel({
        offset: new AMap.Pixel(20, 20),
        content: str
    });

    markerStart.setMap(map);
}

function addMarkerEnd(lon, lat, str) {
    if (markerEnd != null) {
        markerEnd.setMap(null);
    }

    markerEnd = new AMap.Marker({
        position: [lon, lat],
        icon: "https://webapi.amap.com/theme/v1.3/markers/n/mark_r.png",
    });

    markerEnd.setLabel({
        offset: new AMap.Pixel(20, 20),
        content: str
    });

    markerEnd.setMap(map);
}


function navigation(startPoint, endPoint) {

    map.clearMap();

    //构造路线导航类
    AMap.service(["AMap.Walking"], function () {
        var MWalk = new AMap.Walking({
            map: map,
        });

        //根据起终点坐标规划步行路线
        MWalk.search(startPoint, endPoint, function (
            status, result) {
            if (status == 'complete') {

                len = result.routes['0']['steps'].length;
                lonArr.length = len;
                latArr.length = len;

                for (var i = 0; i < len; i++) {
                    lonArr[i] = (result.routes[0]['steps'][i].end_location['lng']);
                    latArr[i] = (result.routes[0]['steps'][i].end_location['lat']);
                }

                msg = new ROSLIB.Message({
                    success: true,
                    lon: lonArr,
                    lat: latArr
                });

                console.log(msg);
                window.route.publish(msg);
            }
        });
    });
}