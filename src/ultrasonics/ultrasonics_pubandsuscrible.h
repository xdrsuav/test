#ifndef ULTRASONICS_VECHILE_H
#define ULTRASONICS_VECHILE_H
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "roscommunicate.h"
#include <memory>

class ultrasonic_pubandsuscrible
{
public:
    ultrasonic_pubandsuscrible();

private:
    ros::NodeHandle nh_;
    ros::Publisher pub_;
    ros::Subscriber sub_;
    std::vector<int>vecout;
    double angle;
    double speed;
    void cmdMsgReceCallBack(const geometry_msgs::Twist::ConstPtr &msg);
    std::shared_ptr<roscommunicate>communicate_;
    const std::string cmd = "55AA010101";

};

#endif // ULTRASONICS_VECHILE_H
