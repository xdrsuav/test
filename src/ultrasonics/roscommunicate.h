#ifndef ROSCOMMUNICATE_H
#define ROSCOMMUNICATE_H
#include <string>
#include <iostream>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include "common.h"
#include <vector>

class roscommunicate
{
public:
    enum comm_stopbits_t
    {
        COMM_ONESTOPBIT,	///< 1 stop bit
        COMM_ONE5STOPBITS,	///< 1.5 stop bit
        COMM_TWOSTOPBITS	///< 2 stop bit
    };

    enum comm_parity_t
    {
        COMM_NOPARITY,	///< No parity
        COMM_ODDPARITY,	///< Odd parity
        COMM_EVENPARITY,///< Even parity
        COMM_MARK,		///<
        COMM_SPACE		///<
    };
    typedef struct com_attr       /*我自己定义的串口属性结构*/
    {
        unsigned int baudrate;    /*波特率*/
        unsigned char databits;    /*数据位*/
        unsigned char stopbits;    /*停止位*/
        unsigned char parity;    /*校验位*/
    }com_attr;

    roscommunicate(std::string _strPath,unsigned int _baudrate,unsigned char _databits,unsigned char _stopbits,unsigned char _parity);
    ~roscommunicate();
    int init();
    bool openDevice();
    int sendhex(std::string hexcmd,int length);
    int sendascii(std::string strcmd,int length);
    unsigned char* getData(int length);
    void parseData(unsigned char* input,std::vector<int>&output);
    void closeDevice();


private:
    int fd_;
    com_attr attr;
    std::string strPath;
    unsigned char *outbuf;


};
#endif // ROSCOMMUNICATE_H
