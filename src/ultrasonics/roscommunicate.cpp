#include "roscommunicate.h"
#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <iostream>
#include <thread>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

roscommunicate::roscommunicate(std::string _strPath,unsigned int _baudrate,unsigned char _databits,unsigned char _stopbits,unsigned char _parity)
{
  strPath = _strPath;
  attr.baudrate = _baudrate;
  attr.databits = _databits;
  attr.stopbits = _stopbits;
  attr.parity = _parity;
  fd_ = -1;
  outbuf = new unsigned char[30];
  memset(outbuf,0,30);
}

roscommunicate::~roscommunicate()
{
    delete [] outbuf;
    if(fd_ !=-1){
        printf("close Device\n");
        closeDevice();
    }

}

int roscommunicate::init()
{
  termios opt;
  /*******************波特率********************/
  printf("set baudrate %d\n", attr.baudrate);
  switch (attr.baudrate)
  {
  case 2400:
    cfsetispeed(&opt, B2400);
    cfsetospeed(&opt, B2400);
    break;
  case 4800:
    cfsetispeed(&opt, B4800);
    cfsetospeed(&opt, B4800);
    break;
  case 9600:
    cfsetispeed(&opt, B9600);
    cfsetospeed(&opt, B9600);
    break;
  case 19200:
    cfsetispeed(&opt, B19200);
    cfsetospeed(&opt, B19200);
    break;
  case 38400:
    cfsetispeed(&opt, B38400);
    cfsetospeed(&opt, B38400);
    break;
  case 57600:
    cfsetispeed(&opt, B57600);
    cfsetospeed(&opt, B57600);
    break;
  case 115200:
    cfsetispeed(&opt, B115200);
    cfsetospeed(&opt, B115200);
    break;

  default:
    printf("unsupported baudrate %d\n", attr.baudrate);
    return -1;
    break;
  }


  /*******************停止位***************/
  switch (attr.stopbits)
  {
  case COMM_ONESTOPBIT:
    opt.c_cflag &= ~CSTOPB;
    break;
  case COMM_ONE5STOPBITS:
    break;

  case COMM_TWOSTOPBITS:
    opt.c_cflag |= CSTOPB;
    break;
  default:
    printf("unsupported stop bits %d\n", attr.stopbits);
    return -1;
    break;
  }

  opt.c_iflag &= ~(ICRNL | INLCR);
  opt.c_iflag &= ~(IXON | IXOFF | IXANY);/*关闭软件流控(一般都是关闭软硬流控，我也不知道为啥)*/


  tcflush(fd_, TCIOFLUSH);        //刷清缓冲区
  if (tcsetattr(fd_, TCSANOW, &opt) < 0)
  {
    printf("tcsetattr faild\n");
    return -1;
  }
  return 0;
}

bool roscommunicate::openDevice()
{
  fd_ = open(strPath.c_str(),O_RDWR | O_NOCTTY);
  return fd_ != -1;
}

int roscommunicate::sendhex(std::string hexcmd, int length)
{
  int ret;
  unsigned char  *buff = new unsigned char[length];
  memset(buff,0,length);
  memcpy(buff,hexcmd.c_str(),length);
  unsigned char *outdata = new unsigned char[length];
  string2hex(buff,length,outdata);

  if(fd_ <0)
  {
    printf("device open error\n");
    return -1;
  }
  ret = write(fd_,outdata,length/2);
  return ret;
}

int roscommunicate::sendascii(std::string strcmd,int length)
{
  int ret;
  if(fd_ < 0)
  {
    printf("device open error\n");
    return -1;
  }
  ret = write(fd_, strcmd.c_str(), length);
  return ret;
}

unsigned char*roscommunicate::getData(int length)
{
  int len,fs_sel;
  fd_set fs_read;

  struct timeval time;

  FD_ZERO(&fs_read);
  FD_SET(fd_,&fs_read);

  time.tv_sec = 10;
  time.tv_usec = 0;

  fs_sel = select(fd_+1,&fs_read,NULL,NULL,&time);

  int ret;
  if(fd_ < 0)
  {
    printf("com opened error");
    exit(0);
  }
  usleep(30000);
  ret = read(fd_, outbuf, 20);
  std::cout<<ret<<std::endl;
  return outbuf;
}

void roscommunicate::parseData(unsigned char *input, std::vector<int> &output)
{

  output.clear();
  unsigned char bufhead[] = {0x55,0xaa};

  int ret = memcmp(input,bufhead,2);
  if(ret !=0){
    printf("data error\n");
    return ;
  }
  int iPro1 = input[4]<<8 |input[5];
  output.push_back(iPro1);
  int iPro2 = input[6]<<8 |input[7];
  output.push_back(iPro2);
  int iPro3 = input[8]<<8 |input[9];
  output.push_back(iPro3);
  int iPro4 = input[10] <<8 | input[11];
  output.push_back(iPro4);

}

void roscommunicate::closeDevice()
{
  close(fd_);
}


