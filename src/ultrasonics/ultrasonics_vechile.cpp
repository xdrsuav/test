#include "ros/ros.h"
#include "std_msgs/String.h"
#include "roscommunicate.h"
#include <sstream>
#include <geometry_msgs/Twist.h>
#include "ultrasonics_pubandsuscrible.h"

std::string cmd = "55AA010101";

static double vecle, angle;

roscommunicate communicate("",9600,8,1,0);

void cmdMsgReceCallBack(const geometry_msgs::Twist::ConstPtr &msg)
{

    vecle = msg->linear.x;
    angle = msg->angular.z;
    vecle = fabs(vecle) * 10 * 3.6 + OFFSET_VEL;                // m/s => km/h resolution:0.1 default value:1000
    angle = ((angle - 0.107) * 180 / M_PI) * 10 + OFFSET_ANGLE; //offset :-90,resolution: 0.1 default value :900

    unsigned char*output = new unsigned char[30];
    memset(output,0,30);
    communicate.sendhex(cmd,cmd.length());
    output = communicate.getData(30);
    printf("output:%d\n",output[12]);
    std::vector<int> vecout;
    communicate.parseData(output,vecout);
    std::stringstream ss;
    for(auto vec:vecout){


        if(vec<2000){
            vecle = 0;
        }

    }
    vecle = 10;
    angle = 20;



}


//int main(int argc, char **argv)
//{
//  ros::init(argc, argv, "ultrasonics_vechile");
//  std::shared_ptr<ultrasonic_pubandsuscrible >ultr_pub_ = std::make_shared<ultrasonic_pubandsuscrible>();

//  ros::NodeHandle nh = ros::NodeHandle();

//  ros::Publisher ultrasonic_pub = nh.advertise<std_msgs::String>("ultrasonics_distance", 1000);
//  ros::Subscriber ultrasonic_sub = nh.subscribe<geometry_msgs::Twist>("/cmd_vel",1,cmdMsgReceCallBack,nullptr);


//  bool bret = communicate.openDevice();
//  if(!bret){
//      printf("openDevice failed\n");
//      return -1;
//  }

//  int iret = communicate.init();
//  if(iret != 0){
//      printf("init Device failed\n");
//      return -1;
//  }



//  ros::Rate loop_rate(10);
//  while (ros::ok())
//  {

//    unsigned char*output = new unsigned char[30];
//    memset(output,0,30);
//    communicate.sendhex(cmd,cmd.length());
//    output = communicate.getData(30);
//    printf("output:%d\n",output[12]);
//    std::vector<int> vecout;
//    communicate.parseData(output,vecout);

//    for(auto vec:vecout){


//      ss<<vec<<",";

//    }
//      std::stringstream ss;
//      std_msgs::String msg;
//      ss<<"vecle:"<<vecle<<"angle:"<<angle;
//      msg.data = ss.str();

//      ultrasonic_pub.publish(msg);
//      ros::spinOnce();

//      loop_rate.sleep();
//  }

//  return 0;
//}
