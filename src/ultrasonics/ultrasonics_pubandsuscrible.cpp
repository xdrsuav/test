#include "ultrasonics_pubandsuscrible.h"
#include <std_msgs/String.h>
#include "roscommunicate.h"
#include <vector>
#include <sstream>
#include <thread>

ultrasonic_pubandsuscrible::ultrasonic_pubandsuscrible()
{
    communicate_ = std::make_shared<roscommunicate>("/dev/ttyUSB0",9600,8,1,0);

    communicate_->openDevice();
    communicate_->init();

    sub_ = nh_.subscribe<geometry_msgs::Twist>("/cmd_vel",1,&ultrasonic_pubandsuscrible::cmdMsgReceCallBack,this);
    pub_ = nh_.advertise<geometry_msgs::Twist>("ultrasonics_distance",1000);



}

void ultrasonic_pubandsuscrible::cmdMsgReceCallBack(const geometry_msgs::Twist::ConstPtr &msg)
{
    geometry_msgs::Twist msgs;
    unsigned char*output = new unsigned char[13];
    memset(output,0,13);
    communicate_->sendhex(cmd,cmd.length());
    output = communicate_->getData(13);

    communicate_->parseData(output,vecout);
    std::cout<<"size:"<<vecout.size()<<std::endl;
    usleep(30000);

    if(vecout.size()==0)
    {
        std::cout<<"size = 0" <<std::endl;
        return;
    }

    angle = msg->angular.z;
    std::cout<<"speed: "<<speed<<std::endl;
   for(auto vec:vecout){
       if(vec<1500 && vec!=0)
        {
            speed = 0.;
            msgs->linear.y = 1;
        }   
        else
        {
            speed = msg->linear.x;
            msgs->linear.y = 0;
        }
            
           
           
   }

    int upgrade = msg->linear.z;
    
    msgs.linear.x = speed;
    msgs.linear.z = upgrade;
    msgs.angular.z = angle;
    pub_.publish(msgs);
}

int main(int argc,char**argv)
{
    ros::init(argc,argv,"ultrason_pub");
    ultrasonic_pubandsuscrible up;


    ros::spin();
    return 0;

}


