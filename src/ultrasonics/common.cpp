#include "common.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void string2hex(unsigned char* _input,size_t length, unsigned char *_out)
{
    char temp[3] = {0};
    unsigned char result;
    for (int i = 0;i<length;i++){
        if(_input[i]<'0' ||_input[i]>'f'){
            printf("data format error\n");
            return ;
        }

    }

    int hexlen = length/2;
    int offset = 0;
    memset(_out,0,20);
    for(int j = 0;j<hexlen;j++)
    {
        memset(temp,0,3);
        memcpy(temp,_input + offset,2);
        result = strtoul(temp,NULL,16);
        *(_out+j) = result;
        offset +=2;
    }

}
