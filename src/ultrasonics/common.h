#ifndef COMMON_H
#define COMMON_H
#include <sstream>
#include <string>
#include <thread>

#define OFFSET_VEL 1000
#define OFFSET_ANGLE 900

void string2hex(unsigned char* _input,size_t length, unsigned char *_out);
#endif // COMMON_H
