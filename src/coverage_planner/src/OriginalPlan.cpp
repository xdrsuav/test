// header
#include <iostream>
#include "nav_msgs/Path.h"
#include "geometry_msgs/PolygonStamped.h"
#include <OriginalPlan.hpp>

ros::Publisher polygonPub_;
ros::Publisher subPolygonPub_;
ros::Publisher pathPub_;

/**
 * @brief Generate vector of polygon from PointVector
 * @param polygon
 * @return std::vector<geometry_msgs::Polygon> Vector of subpolygons assumed to be passed to ROS msg
 */
std::vector<geometry_msgs::Polygon> generatePolygonVector(const PointVector &polygon)
{
    // convert PointVector (a.k.a. std::vector<geometry_msgs::Point>) to geometry_msgs::Polygon
    // so that polygon is visualized on the window
    geometry_msgs::Polygon poly;
    for (const auto &vertex : polygon)
    {
        geometry_msgs::Point32 p;
        p.x = vertex.x;
        p.y = vertex.y;
        poly.points.push_back(p);
    }
    geometry_msgs::PolygonStamped polyStamped;
    polyStamped.header.frame_id = "map";
    polyStamped.polygon = poly;
    polygonPub_.publish(polyStamped);

    std::vector<geometry_msgs::Polygon> polygons = {poly};
    return polygons;
}

/**
 * @brief Generate vector of polygon from std::vector<PointVector>
 * @param subPolygons
 * @return std::vector<geometry_msgs::Polygon> Vector of subpolygons assumed to be passed to ROS msg
 */
std::vector<geometry_msgs::Polygon> generatePolygonVector(const std::vector<PointVector> &subPolygons)
{
    std::vector<geometry_msgs::Polygon> subPolygonsRet;

    // convert PointVector (a.k.a. std::vector<geometry_msgs::Point>) to geometry_msgs::Polygon
    // so that polygon is visualized on the window
    for (const auto &subPolygon : subPolygons)
    {
        geometry_msgs::Polygon poly;
        for (const auto &vertex : subPolygon)
        {
            geometry_msgs::Point32 pt;
            pt.x = vertex.x;
            pt.y = vertex.y;
            poly.points.push_back(pt);
        }
        subPolygonsRet.push_back(poly);
    }

    return subPolygonsRet;
}

/**
 * @brief Plans coverage path
 * @param req Contains values neccesary to plan a path
 * @param res Contains resulting path
 * @return bool
 * @details For details of this service, cf. srv/PolygonToPath.srv
 */
using  namespace  std;
void print(coverage_planner::PolygonToPath::Request req)
{

    cout << "footprintLength = " << req.footprint_length << endl;
    cout<<"footprintWidth = "<<req.footprint_width<<endl;
    cout<<"horizontalOverwrap = "<<req.horizontal_overwrap<<endl;
    cout<<"verticalOverwrap = "<<req.vertical_overwrap<<endl;
    cout << "start : " << req.start << endl;
    for (int j = 0; j <req.polygon.size() ; j++) {
        cout << "polygon  " << j << ":" << req.polygon[j] << endl;
    }

}
bool plan(coverage_planner::PolygonToPath::Request &req, coverage_planner::PolygonToPath::Response &res)
{

    // see torres et al. 2016 for the flow of this algorithm
    print(req);
    // polygon from request and path for response
    PointVector polygon, candidatePath;
    // start point of coverage path
    geometry_msgs::Point start;
    // parameters of coverage path
    std_msgs::Float64 footprintLength, footprintWidth, horizontalOverwrap, verticalOverwrap;

    polygon = req.polygon;
    start = req.start;

    footprintLength = req.footprint_length;
    footprintWidth = req.footprint_width;
    // footprintWidth.data = 12;
    horizontalOverwrap = req.horizontal_overwrap;
    verticalOverwrap = req.vertical_overwrap;

    // isOptimal is true if computed path does not have intersection with polygon
    bool isOptimal = computeConvexCoverage(polygon, footprintWidth.data, horizontalOverwrap.data, candidatePath);

    // if (candidatePath.size() == 0)
    // {
    //   ROS_ERROR("can't plan a path");
    //   return false;
    // }

    nav_msgs::Path coveragePath;
    geometry_msgs::PoseStamped pointBuf;
    if (1) //isOptimal == true
    {
        // fill "subpolygon" field of response so that polygon is visualized
        res.subpolygons = generatePolygonVector(polygon);

        // set optimal alternative as optimal path
        // see torres et al. 2016 for Optimal Alternative
        res.path = identifyOptimalAlternative(polygon, candidatePath, start);
        pointBuf.header.frame_id = "map";
        coveragePath.header.frame_id = "map";
        for (size_t i = 0; i < res.path.size(); i++)
        {
            pointBuf.pose.position.x = res.path[i].x;
            pointBuf.pose.position.y = res.path[i].y;
            pointBuf.pose.position.z = res.path[i].z;
            coveragePath.poses.push_back(pointBuf);
        }
        geometry_msgs::PolygonStamped polyStamped;
        polyStamped.header.frame_id = "map";
        for (size_t i = 0; i < res.subpolygons.size(); i++)
        {
            polyStamped.polygon = res.subpolygons[res.subpolygons.size() - 1];
            subPolygonPub_.publish(polyStamped);
        }
        // interpolateToPath(coveragePath, 1);
        pathPub_.publish(coveragePath);
    }
    else
    {

        std::vector<PointVector> subPolygons = decomposePolygon(polygon);

        // sum of length of all coverage path
        double pathLengthSum = 0;

        int i = 1;
        // compute length of coverage path of each subpolygon
        for (const auto &polygon : subPolygons)
        {
            PointVector partialPath;
            computeConvexCoverage(polygon, footprintWidth.data, horizontalOverwrap.data, partialPath);
            pathLengthSum += calculatePathLength(partialPath);
        }

        // existsSecondOptimalPath is true if there is at least one coverage that has no intersection with polygon
        // second optimal path is the path that has second shortest sweep direction and no intersection with polygon
        PointVector secondOptimalPath;
        bool existsSecondOptimalPath =
                findSecondOptimalPath(polygon, footprintWidth.data, horizontalOverwrap.data, candidatePath);

        if (existsSecondOptimalPath == true)
        {
            // compute optimal alternative for second optimal path
            secondOptimalPath = identifyOptimalAlternative(polygon, candidatePath, start);

            // if the length of second optimal path is shorter than the sum of coverage path of subpolygons,
            // set second optimal path as the path
            if (pathLengthSum > calculatePathLength(secondOptimalPath))
            {
                // fill "subpolygon" field of response so that polygon is visualized
                res.subpolygons = generatePolygonVector(polygon);

                res.path = secondOptimalPath;

                return true;
            }
                // returns second optimal path when shortest path is not optimal and polygon cannot be decomposed
            else if (subPolygons.size() == 1)
            {
                res.subpolygons = generatePolygonVector(polygon);

                res.path = secondOptimalPath;

                return true;
            }
        }
        else if (subPolygons.size() < 2)
        {
            // if number of subpolygon is smaller than 2,
            // it means no valid path can be computed
            ROS_ERROR("Unable to generate path.");
            return true;
        }

        // fill "subpolygon" field of response so that polygon is visualized
        res.subpolygons = generatePolygonVector(subPolygons);

        // compute coverage path of subpolygons
        PointVector multipleCoveragePath =
                computeMultiplePolygonCoverage(subPolygons, footprintWidth.data, horizontalOverwrap.data);

        res.path = multipleCoveragePath;
        // nav_msgs::Path coveragePath;
        // geometry_msgs::PoseStamped pointBuf;
        pointBuf.header.frame_id = "map";
        coveragePath.header.frame_id = "map";
        for (size_t i = 0; i < multipleCoveragePath.size(); i++)
        {
            pointBuf.pose.position.x = multipleCoveragePath[i].x;
            pointBuf.pose.position.y = multipleCoveragePath[i].y;
            pointBuf.pose.position.z = multipleCoveragePath[i].z;
            coveragePath.poses.push_back(pointBuf);
        }
        geometry_msgs::PolygonStamped polyStamped;
        polyStamped.header.frame_id = "map";
        polyStamped.polygon = res.subpolygons[0];
        subPolygonPub_.publish(polyStamped);
        // interpolateToPath(coveragePath, 1);
        pathPub_.publish(coveragePath);
    }

    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "CoveragePath");
    ros::NodeHandle nh;

    ros::ServiceServer planner = nh.advertiseService("cpp_PolygonToPath", plan);
    polygonPub_ = nh.advertise<geometry_msgs::PolygonStamped>("polygons", 1);
    subPolygonPub_ = nh.advertise<geometry_msgs::PolygonStamped>("/subpolygons", 1);
    pathPub_ = nh.advertise<nav_msgs::Path>("coverage_path", 1);
    ROS_INFO("Ready to plan.");
 


    ros::spin();

    return 0;
}
