/*
 * @Author: your name
 * @Date: 2020-06-16 19:16:31
 * @LastEditTime: 2020-06-22 01:26:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /uav_master/src/coverage_planner-master/src/planner.cpp
 */

#include <iostream>
#include "ros/ros.h"
#include "conversions.h"
#include "Planner.h"
#include "document.h"
#include "stringbuffer.h"
#include "prettywriter.h"
#include "conversions.h"

using namespace rapidjson;
using namespace std;

Planner::Planner()
{
    plannerClient_ = nh_.serviceClient<coverage_planner::PolygonToPath>("cpp_PolygonToPath");
    originalCoveragePathPub_ = nh_.advertise<nav_msgs::Path>("original_path", 2);
    finalCoveragePathPub_ = nh_.advertise<nav_msgs::Path>("/satellite_map/utm_coverage_path_final", 2);
    carCmdPub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    gpsCoveragePathFinalPub_ = nh_.advertise<std_msgs::String>("/satellite_map/gps_coverage_path_final", 1000);

    // gazebo rviz sim
    // rvizPolygonSub_ = nh_.subscribe("/navclicks", 10, &Planner::rvizPolygonMsgCallback, this);
    // rvizNavToPointSub_ = nh_.subscribe("move_base_simple/goal", 1, &Planner::rvizGoalMsgCallback, this);
    // rvizRefreshSub_ = nh_.subscribe("/Refresh", 5, &Planner::rvizRefreshMsgCallback, this);
    // gazeboCurrentPoseSub_ = nh_.subscribe("/current_pose", 10, &Planner::gazeboCurrentPoseMsgCallback, this);

    //bige map
    gpsPolygonSub_ = nh_.subscribe("/satellite_map/gps_polygon", 5, &Planner::gpsPolygonMsgCallback, this);
    gpsGoalSub_ = nh_.subscribe("/satellite_map/gps_target", 1, &Planner::gpsTargetMsgCallback, this);
    webStartMsgSub_ = nh_.subscribe("/satellite_map/start", 1, &Planner::webStartMsgCallback, this);
    webStopMsgSub_ = nh_.subscribe("/satellite_map/stop", 1, &Planner::webStopMsgCallback, this);
    gpsCurrentPoseSub_ = nh_.subscribe("/sensor/gps", 1, &Planner::gpsCurrentPoseMsgCallback, this);

    turningPointer_ = new Turning();
    trackingPointer_ = new Tracking(0.3, 0.36, 3);

    offset_x_ = 0.0;
    offset_y_ = 0.0;
    resetPlannerParam();
}

Planner::~Planner()
{
    if (turningPointer_)
    {
        delete turningPointer_;
    }
    if (trackingPointer_)
    {
        delete trackingPointer_;
    }
}

void Planner::rvizGoalMsgCallback(const geometry_msgs::PoseStamped &msg)
{
    ROS_INFO("Nav to one point mode!");
    goal_ = msg;
    isNavToPointMode_ = true;
    isGenerateFinalPathSuccessful_ = false;
}

void Planner::rvizPolygonMsgCallback(const geometry_msgs::PointStamped &msg)
{
    geometry_msgs::Point pBuf;
    pBuf.x = msg.point.x;
    pBuf.y = msg.point.y;
    srv_.request.polygon.push_back(pBuf);
}
void Planner::publishStringPath(const nav_msgs::Path &msg)
{
    //    int j=msg.poses.size();
    //    string pixels[j];
    //    ros::NodeHandle nk;
    //    std_msgs::String gpss;
    //    for (int i=0;i<j;i++)
    //    {
    //        geometry_msgs::PoseStamped tmp;
    //        tmp=msg.poses[i];
    //        const char json[]= "{\"lat\":null,\"lng\":null}";
    //        Document d;
    //        d.Parse(json);
    //        double utm_e = tmp.pose.position.x;
    //        double utm_n = tmp.pose.position.y;
    //        double gps_lat ;
    //        double gps_lng ;
    //        char zone = 0;
    //        gps_common::UTMtoLL(utm_n,utm_e,&zone,gps_lat,gps_lng);
    //        d["lat"]=gps_lat;
    //        d["lng"]=gps_lng;
    //        StringBuffer sb;
    //        PrettyWriter<StringBuffer> writer(sb);
    //        d.Accept(writer);
    //        pixels[i]=sb.GetString();
    //        ROS_INFO("%s",pixels[i].c_str());
    //        std_msgs::String gps;
    //        gps.data=pixels[i];
    //        gpsCoveragePathFinalPub_.publish(gps);
    //    }
}
void Planner::rvizRefreshMsgCallback(const std_msgs::Int8 &msg)
{
    std::cout << "refresh" << std::endl;
    switch (msg.data)
    {
    case 11:
        if (!isNavToPointMode_)
        {
            srv_.request.start.x = currentPose_.pose.position.x;
            srv_.request.start.y = currentPose_.pose.position.y;
            if (callService())
            {
                finalPath_ = turningPointer_->generateFinalPath(originalPath_, currentPose_);
                finalCoveragePathPub_.publish(finalPath_);
                isGenerateFinalPathSuccessful_ = true;
            }
        }
        isReadyToRun_ = true;
        std::cout << isReadyToRun_ << "/" << isNavToPointMode_ << "/" << isGenerateFinalPathSuccessful_ << std::endl;

        break;
    case 33:
        resetPlannerParam();
        break;
    default:
        break;
    }
}

void Planner::gazeboCurrentPoseMsgCallback(const geometry_msgs::PoseStamped &msg)
{
    currentPose_ = msg;
    if (!isReadyToRun_)
    {
        return;
    }
    if (isNavToPointMode_)
    {

        if (!trackingPointer_->run(currentPose_, goal_))
        {
            resetPlannerParam();
        }
        return;
    }
    else if (isGenerateFinalPathSuccessful_)
    {
        if (!(trackingPointer_->run(currentPose_, finalPath_)))
        {
            resetPlannerParam();
        }
    }
}

void Planner::gpsPolygonMsgCallback(const std_msgs::String &msg)
{
    resetPlannerParam();
    std::string s = msg.data.c_str();
    ROS_INFO("I heard: [%s]", msg.data.c_str());
    rapidjson::Document doc;
    //首先进行解析，没有解析错误才能进行具体字段的解析
    if (!doc.Parse(s.data()).HasParseError())
    {
        //5.5 结构体数组类型
        if (doc.HasMember("shapes") && doc["shapes"].IsArray())
        {
            const rapidjson::Value &array = doc["shapes"];
            size_t len = array.Size();
            for (size_t i = 0; i < len; i++)
            {
                const rapidjson::Value &object = array[i];
                size_t len2 = object.Size();

                for (size_t j = 0; j < len2; j++)
                {
                    const rapidjson::Value &object2 = object[j];
                    double lat = 0.0;
                    double lng = 0.0;
                    if (object2.IsObject())
                    {
                        cout << "ObjectArray[" << j << "]: ";
                        if (object2.HasMember("lat") && object2["lat"].IsDouble())
                        {
                            lat = object2["lat"].GetDouble();
                            cout << "lat=" << object2["lat"].GetDouble();
                        }
                        if (object2.HasMember("lng") && object2["lng"].IsDouble())
                        {
                            lng = object2["lng"].GetDouble();
                            cout << ", lng=" << object2["lng"].GetDouble() << endl;
                        }
                        double UTM_N;
                        double UTM_E;
                        std::string zone;
                        gps_common::LLtoUTM(lat, lng, UTM_N, UTM_E, zone);
                        geometry_msgs::Point pBuf;
                        pBuf.x = UTM_E;
                        pBuf.y = UTM_N;
                        if (j == 0)
                        {
                            offset_x_ = UTM_E - 100.0;
                            offset_y_ = UTM_N - 100.0;
                        }

                        pBuf.x = UTM_E - offset_x_;
                        pBuf.y = UTM_N - offset_y_;

                        srv_.request.polygon.push_back(pBuf);
                        cout << "srv_x:" << pBuf.x << endl;
                        cout << "srv_y:" << pBuf.y << endl;
                    }
                }
                // srv_.request.start.x = srv_.request.polygon[0].x;
                // srv_.request.start.y = srv_.request.polygon[0].y;
            }
        }
    }
}
void Planner::gpsTargetMsgCallback(const std_msgs::String &msg)
{
    ROS_INFO("Nav to one point mode!");
    goal_ = transformTargetFromGps2Utm(msg);
    isNavToPointMode_ = true;
    isGenerateFinalPathSuccessful_ = false;
}
void Planner::gpsCurrentPoseMsgCallback(const sensor_msgs::NavSatFix &msg)
{
    if (msg.status.status != 4)
    {
        ROS_INFO("Missed gps data");
        return;
    }

    currentPose_ = transformCurrentPoseFromGps2Utm(msg);
    if (!isReadyToRun_)
    {
        return;
    }
    if (isNavToPointMode_)
    {

        if (!trackingPointer_->run(currentPose_, goal_))
        {
            resetPlannerParam();
        }
        return;
    }
    else if (isGenerateFinalPathSuccessful_)
    {
        if (!(trackingPointer_->run(currentPose_, finalPath_)))
        {
            resetPlannerParam();
        }
    }
}
void Planner::pubGPSCoveragePathFinal(const nav_msgs::Path &msg)
{
    int pathLength = msg.poses.size();
    Document doc;
    Document::AllocatorType &allocator = doc.GetAllocator();
    doc.SetObject();
    Value array(kArrayType);
    for (int i = 0; i < pathLength; i++)
    {
        Value item(kObjectType);
        double utm_e = msg.poses[i].pose.position.x;
        double utm_n = msg.poses[i].pose.position.y;
        double gps_lat;
        double gps_lng;
        char *zone = "50S";
        gps_common::UTMtoLL(utm_n, utm_e, zone, gps_lat, gps_lng);
        item.AddMember("lat", gps_lat, allocator);
        item.AddMember("lng", gps_lng, allocator);
        if (i % 10 == 0)
        {
            array.PushBack(item, allocator);
        }
    }
    doc.AddMember("shapes", array, allocator);
    StringBuffer stringBuf;
    PrettyWriter<StringBuffer> writer(stringBuf);
    doc.Accept(writer);
    std_msgs::String gps;
    gps.data = stringBuf.GetString();
    cout << "gps:" << gps.data << endl;
    ROS_INFO("publish GPS path");
    gpsCoveragePathFinalPub_.publish(gps);
}

void Planner::webStartMsgCallback(const std_msgs::String &msg)
{
    ROS_INFO("Start to run");
    if (!isNavToPointMode_)
    {
        //TODO
        srv_.request.start.x = currentPose_.pose.position.x;
        srv_.request.start.y = currentPose_.pose.position.y;
        if (callService())
        {
            finalPath_ = turningPointer_->generateFinalPath(originalPath_, currentPose_);
            finalCoveragePathPub_.publish(finalPath_);
            pubGPSCoveragePathFinal(finalPath_);
            isGenerateFinalPathSuccessful_ = true;
        }
    }
    isReadyToRun_ = true;
    std::cout << isReadyToRun_ << "/" << isNavToPointMode_ << "/" << isGenerateFinalPathSuccessful_ << std::endl;
}
void Planner::webStopMsgCallback(const std_msgs::String &msg)
{
    resetPlannerParam();
    ROS_INFO("Stop and init");
}
geometry_msgs::PoseStamped Planner::transformTargetFromGps2Utm(std_msgs::String gps_target)
{
    std::string s = gps_target.data.c_str();
    ROS_INFO("I heard: [%s]", gps_target.data.c_str());
    rapidjson::Document doc;
    double lat = 0.0;
    double lng = 0.0;
    //首先进行解析，没有解析错误才能进行具体字段的解析
    if (!doc.Parse(s.data()).HasParseError())
    {
        const rapidjson::Value &json_lat = doc["lat"];
        lat = json_lat.GetDouble();
        cout << "lat:" << lat << endl;
        const rapidjson::Value &json_lng = doc["lng"];
        lng = json_lng.GetDouble();
        cout << "lng:" << lng << endl;
    }
    double N;
    double E;
    geometry_msgs::PoseStamped target;
    std::string ss;
    gps_common::LLtoUTM(lat, lng, N, E, ss);
    target.pose.position.x = E;
    target.pose.position.y = N;
    cout << "target_x/target_y" << target.pose.position.x << "/" << target.pose.position.y << endl;
    return target;
}
geometry_msgs::PoseStamped Planner::transformCurrentPoseFromGps2Utm(sensor_msgs::NavSatFix gps_current_pose)
{
    geometry_msgs::PoseStamped curPose;
    double N, E;
    double heading;
    std::string ss;
    gps_common::LLtoUTM(gps_current_pose.latitude, gps_current_pose.longitude, N, E, ss);
    //    heading = angleDiff(PI / 2, msg.altitude);
    curPose.header.frame_id = "map";
    curPose.pose.position.x = E;
    curPose.pose.position.y = N;
    curPose.pose.orientation = tf::createQuaternionMsgFromYaw((90.0 - gps_current_pose.altitude) / 180.0 * M_PI);
    return curPose;
}

void Planner::resetPlannerParam()
{
    isGenerateFinalPathSuccessful_ = false;
    isNavToPointMode_ = false;
    isReadyToRun_ = false;
    srv_.request.footprint_length.data = 6.69422; //default 16.74/6.69422
    srv_.request.footprint_width.data = 3.34711;  //default 6 //3.34711
    srv_.request.horizontal_overwrap.data = 0.3;  //0.3
    srv_.request.vertical_overwrap.data = 0.2;
    srv_.request.start.x = 0; //39.375
    srv_.request.start.y = 0; //77.9167
    srv_.request.polygon.clear();
    originalPath_.poses.clear();
    finalPath_.poses.clear();
    originalPath_.header.frame_id = "map";
    finalPath_.header.frame_id = "map";
    cmd_vel_.linear.x = 0;
    cmd_vel_.angular.z = 0;
    carCmdPub_.publish(cmd_vel_);
    finalCoveragePathPub_.publish(finalPath_);
}

bool Planner::callService()
{
    if (plannerClient_.call(srv_))
    {
        if (srv_.response.path.empty())
        {
            ROS_INFO("Please click to choose input vertex---");
            return false;
        }
        geometry_msgs::PoseStamped pointBuf;
        pointBuf.header.frame_id = "map";
        originalPath_.header.frame_id = "map";
        for (size_t i = 0; i < srv_.response.path.size(); i++)
        {
            pointBuf.pose.position.x = srv_.response.path[i].x + offset_x_;
            pointBuf.pose.position.y = srv_.response.path[i].y + offset_y_;
            pointBuf.pose.position.z = srv_.response.path[i].z;
            originalPath_.poses.push_back(pointBuf);
        }
    }
    else
    {
        ROS_ERROR("Failed to call service cpp_PolygonToPath");
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "planner");
    Planner pl;
    std::cout << "Planner node start" << std::endl;
    ros::spin();
    return 0;
}
