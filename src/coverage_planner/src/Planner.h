/*
 * @Author: your name
 * @Date: 2020-06-16 19:16:00
 * @LastEditTime: 2020-06-19 22:53:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /uav_master/src/coverage_planner-master/src/planner.h
 */

#ifndef PLANNER_H_
#define PLANNER_H_

#include "Turning.h"
#include "Tracking.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int8.h"
#include "std_msgs/Bool.h"
#include "sensor_msgs/NavSatFix.h"
#include "geometry_msgs/PolygonStamped.h"
#include <coverage_planner/PolygonToPath.h>
#include "conversions.h"
#include "rapidjson/document.h"

class Planner
{
public:
    Planner();
    ~Planner();

    //real time
    void gpsPolygonMsgCallback(const std_msgs::String &msg);
    void gpsTargetMsgCallback(const std_msgs::String &msg);
    void gpsCurrentPoseMsgCallback(const sensor_msgs::NavSatFix &msg);
    void webStartMsgCallback(const std_msgs::String &msg);
    void webStopMsgCallback(const std_msgs::String &msg);

   //sim time
    void rvizPolygonMsgCallback(const geometry_msgs::PointStamped &msg);
    void rvizGoalMsgCallback(const geometry_msgs::PoseStamped &msg);
    void rvizRefreshMsgCallback(const std_msgs::Int8 &msg);
    void gazeboCurrentPoseMsgCallback(const geometry_msgs::PoseStamped &msg);

    //operation to srv
    void resetPlannerParam();
    bool callService();
    geometry_msgs::PolygonStamped transformPolygonFromGps2Utm(std_msgs::String gps_polygon);
    void publishStringPath(const nav_msgs::Path& msg);
    geometry_msgs::PoseStamped transformTargetFromGps2Utm(std_msgs::String gps_target);
    geometry_msgs::PoseStamped transformCurrentPoseFromGps2Utm(sensor_msgs::NavSatFix gps_current_pose);
    void pubGPSCoveragePathFinal(const nav_msgs::Path& msg);

    double offset_x_ = 0.0;
    double offset_y_ = 0.0;

private:
    ros::NodeHandle nh_;
    ros::ServiceClient plannerClient_;
    ros::Publisher originalCoveragePathPub_;
    ros::Publisher finalCoveragePathPub_;
    ros::Publisher carCmdPub_;
    ros::Publisher gpsCoveragePathFinalPub_;

    // gazebo rviz sim
    ros::Subscriber rvizPolygonSub_;
    ros::Subscriber rvizNavToPointSub_;
    ros::Subscriber rvizRefreshSub_;
    ros::Subscriber gazeboCurrentPoseSub_;
    // bige map
    ros::Subscriber gpsPolygonSub_;
    ros::Subscriber gpsGoalSub_;
    ros::Subscriber gpsCurrentPoseSub_;
    ros::Subscriber webStartMsgSub_;
    ros::Subscriber webStopMsgSub_;

    coverage_planner::PolygonToPath srv_;
    Turning *turningPointer_;
    Tracking *trackingPointer_;

    geometry_msgs::PoseStamped currentPose_;
    geometry_msgs::PoseStamped goal_;
    geometry_msgs::Twist cmd_vel_;
    nav_msgs::Path originalPath_;
    nav_msgs::Path finalPath_;

    bool isGenerateFinalPathSuccessful_;
    bool isNavToPointMode_;
    bool isReadyToRun_; 
};

#endif
